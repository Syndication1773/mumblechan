require 'rubygems'
require 'mumble-ruby'
require 'net/https'
require 'uri'
require 'cgi'
require 'json'
require 'base64'
require 'open-uri'
require 'wunderground'
require 'time_difference'
require 'thread'
require './hangman'
require 'mechanize'
require 'mini_magick'

class MumbleChan

	$verz = "1.18.6.2"
	$status = nil
	$debug = false

	def get_version ()
		begin
			page = Mechanize.new.get("https://cakezombies.com/mumblechan/changelog.txt").body
			$version = $verz + "<br><br><a href=\"https://cakezombies.com/mumblechan/changelog.txt\">Changelog</a>:<br>"
			if page =~ /^(\#\# \[(\d\.\d{1,2}(\.\d{1,2})?(\.\d{1,2})?).*)%/m
				ver = $1
				magic = $2
				$version << ver.gsub(/\n/,"\n<br>")
				if $verz != magic
					$status = "Mumble-chan is outdated. <br><b>Running</b>: " + $verz + "<br><b>Latest</b>: " + magic
					puts "Mumble-chan is outdated. Running: " + $verz + " Latest: " + magic
					return $version
				else
					$status = "Mumble-chan is up to date. Running " + $verz
				end
			end
			if (@start)
				img = ""
				File.open("./mumbleav.jpg", 'r') { |file| img << file.read }
				@cli.send_user_state(session: @cli.me.session, 
					comment: "Mumble-chan running " + $version + "<br><br><a href=\"https://cakezombies.com/redirect/mumble\">all commands</a><br><a href=\"https://cakezombies.com/logs\">I log the channel I'm in!</a>", 
					texture: img, 
					texture_hash: "1bf588724efeacd19034fea8ea83c1b4ee0f01a4")
			end
		rescue Mechanize::ResponseCodeError => e
			send "Sorry, there was a problem getting the changelog."
			console_msg("HTTP error! " + e.message)
		end	
	end
	
	def log(msg)
		begin
			if !($debug)
				File.open('/home/botmaster/Logs/mumble.log', 'a') { |file| file.write("[" + Time.now.strftime("%H:%M:%S")+"] " + msg+"\n<br>") }
			else
				File.open('/home/botmaster/Logs/mumble_test.log', 'a') { |file| file.write("[" + Time.now.strftime("%H:%M:%S")+"] " + msg+"\n<br>") }
			end
		rescue => e
			puts "Could not open log file, system error. Restarting."
			puts e
			restart()
		end
	end
	
	def log_whisper (msg)
		begin
			File.open('/home/botmaster/Logs/mumble_whisper.log', 'a') { |file| file.write("[" + Time.now.strftime("%H:%M:%S")+"] " + msg+"\n<br>") }
		rescue => e
			puts "Could not open log file, system error. Restarting."
			puts e
			restart()
		end
	end
	
	def restart
		send "Whoops! Something doofed"
		@cli.disconnect
		sleep(15)
		@channel = 1
		bot = MumbleChan.new()
		bot.runtime()
	end

	def send(msg)
		case @channel
		when 4
			chan = 'The Crevice'
		when 1
			chan = 'The Void'
		when 2
			chan = 'Dark Alley'
		when 3
			chan = 'Funky Town'
		when 5
			chan = 'Wildcard'
		end
		if (@channel == 1)
			if !(msg =~ /^<img src='data:image.*/)
				log("Mumble-chan: " + msg)
			end
		end
		begin
			@cli.text_channel chan, msg
		rescue Mumble::ChannelNotFound => e
			puts "User is in an unregistered channel"
		end
	end	

	def send_link (link, title)
		send "<a href=\""+link+"\">"+title+"</a>"
		update_link("Mumble-chan", link, title)
	end
	
	def update_link (user, url, title=nil)
		begin
			if (!(url =~ /^<a href/))
				if (title.nil?)
					url = "<a href=\"#{url}\">#{url}</a>"
				else
					url = "<a href=\"#{url}\">#{title}</a>"
				end
			end
			if !($debug)
				File.open('/home/botmaster/Logs/links.html', 'a') { |file| file.write("[" + Time.now.strftime("%H:%M:%S")+"] " + user +": " + url+ "\n<br>") }
			else
				File.open('/home/botmaster/Logs/links_test.log', 'a') { |file| file.write("[" + Time.now.strftime("%H:%M:%S")+"] " + user + ": " + url+ "\n<br>") }
			end
			console_msg ("#<CakeZomb::LinkUpdate:OK>")
		rescue => e
			puts e
			console_msg ("#<CakeZomb::LinkUpdate:FAIL>")
		end
	end
	
	def kick_user (session_id, reason)
		begin
			@cli.send_user_remove(session: session_id,reason: reason)
		rescue => e
			puts e
		end
	end
	
	def kick_user_srh (user_name, reason)	
		user = @cli.find_user(user_name)
		if (user != nil)
			kick_user(user.session, reason)
			return true
		end
		return false
	end
	
	def flip_coin()
		d = Random.rand(1..100)
		if (d < 25)
			return "Flipped for: heads"
		elsif d < 50
			return "Flipped for: tails"
		elsif d < 75
			return "Flipped for: heads"
		elsif d == 88 || d == 1
			return "Lost the coin!"
		else
			return "Flipped for: tails"
		end	
	end
	
	def slap(person, slapper)
		d = Random.rand(1..10)
		s = slapper + " slapped the shit outta " + person
		if (person.casecmp("mumblechan"))
			if d == 1
				s << "! whatta bitch"
			elsif d == 2
				s << " what a ho"
			elsif d == 3
				s << "! Motherfucking Mumblechan!"
			elsif d == 4
				s << "! Feels so good"
			elsif d == 5
				s << ". ayy lmao"
			elsif d == 6
				s << ". " + slapper + " decided to slap themselves too!"
			elsif d == 7
				s << "... it was a massacre"
			else
				s << "."
			end
			send s
		else
			send "Mumble-chan slapped the hell out of " + slapper
		end
	end

	def encodeImage(url, height, width)
		begin
			fileStrip
			uri = URI.parse(url)
			filename = File.basename(uri.path)
			if (url =~ /.*cakezombies\.com\/memefaces\/.*/i)
				imagedata = open(url) { |io| io.read }
				log "Mumble-chan: <img src=\""+url+"\">"
			elsif (url =~ /^\/home/)
				imagedata = open(url) { |io| io.read }
				log "Mumble-chan: <img src='data:image/*;base64," + Base64.encode64(imagedata) + "' \\>"
			else
				if (url =~ /imgur\.com\/(.*)/)
					url << ".jpg"
					url.sub(/http:/,"https:")
				end
				if (url =~ /.*\.(jpg(.*)?|bmp|jpeg|gif|png)$/)
					size = height+"x"+width
					image = MiniMagick::Image.open(url)
					image.format "jpg"
					image.resize size
					#filename = Time.now.strftime("%H%M%S-%m%d%Y")
					if !(File.file?("/home/botmaster/Mumblechan/tmp#{filename}.jpg"))
						image.write("/home/botmaster/Mumblechan/tmp/#{filename}.jpg")
					end
					imagedata = open("/home/botmaster/Mumblechan/tmp/#{filename}.jpg") { |io| io.read }
					log "Mumble-chan: <img src=\""+url+"\" height=\""+height+"\" width=\""+width+"\">"
				else
					console_msg("#<Mumble::IMGSEND:FAIL> " + url)
					return "Not direct url to image, sorry."
				end
			end
			console_msg("#<Mumble::IMGSEND:OK> " + url)
			return "<img src='data:image/*;base64," + Base64.encode64(imagedata) + "' \\>"
		rescue => e
			puts e
			console_msg "Web server not responding... url: " + url
		end
	end
	
	def fileStrip
		count = Dir[File.join("/home/botmaster/Mumblechan/tmp", '**', '*')].count { |file| File.file?(file) }
		if (count > 36)
			Dir.foreach("/home/botmaster/Mumblechan/tmp") {|f| fn = File.join("/home/botmaster/Mumblechan/tmp", f); File.delete(fn) if f != '.' && f != '..'}
			return false
		end
		return true
	end
	
	def send_img(url, height="450", width="360")
		if (url =~ URI::regexp || url =~ /^\/home/)
			if (url =~ /imgur\.com\/.*/)
				url = url.sub(/http:/,"https:")
			elsif (url =~ /flickr\.com/)
				img = Mechanize.new.get(url).at('.low-res-photo')['src']
				url = "https:" + img
			end
			send encodeImage(url, height, width)
			#update_link("Mumble-chan:", url)
			return true
		else
			return false
		end
	end
	
	def annotate_img (url, caption=nil, meme="default")
		if (caption.nil?)
			send_img (url)
			return true
		elsif (caption.length > 50)
			send "Your meme message is too long, max 50 characters"
			return false
		end
		fileStrip
		pointsize = 40
		length = 12
		if (caption.length > 22)
			pointsize = 28
			length = 17
		end
		text = ""
		image = MiniMagick::Image.open(url)
		caption.gsub!(/'/, '`')
		while (caption.length > length)
			index = caption[0,length-1].rindex(/\s/)
			if (!(index.nil?) && index != 0)
				text << caption[0,index]+"\n"
				caption = caption[index,caption.length-1]
			else
				text << caption[0,length-1] + "\n"
				caption = caption[length-1,caption.length-1]
			end
		end
		image.combine_options do |c|
			c.font "helvetica"
			c.pointsize pointsize
			c.gravity "north"
			c.draw "text 0,25 '#{text}#{caption}'"
			c.fill "#FFFFFF"
		end
		filename = caption.gsub(/[^0-9A-Za-z]/, '')
		if !(File.file?("/home/botmaster/Mumblechan/tmp/meme_#{meme}_#{filename}.jpg"))
			image.write "/home/botmaster/Mumblechan/tmp/meme_#{meme}_#{filename}.jpg"
		end
		console_msg("Sending meme generated image: " + meme)
		if (send_img ("/home/botmaster/Mumblechan/tmp/meme_#{meme}_#{filename}.jpg"))
			return true
		else
			console_msg("Error sending meme")
			return false
		end
	end
	
	def dad_joke ()
		check_time()
		if (@dadjokes.nil?)
			puts "["+ Time.now.strftime("%H:%M:%S") +"] #<DADJOKE::Array:UPDAT>"
			response = Mechanize.new.get("https://www.reddit.com/r/dadjokes/new.json?limit=125").body
			@dadjokes = JSON.parse(response)
		end
		index = Random.rand(1..100)
		str = "<br><b>Title</b>: " + @dadjokes['data']['children'][index]['data']['title'] + "<br><br>"
		str << @dadjokes['data']['children'][index]['data']['selftext'].gsub(/\n/,"<br>")
		send str
	end
	
	def moe ()
		check_time()
		begin
			imt = Thread.new {
				if (@moe.nil?)
					puts "["+ Time.now.strftime("%H:%M:%S") +"] #<MOEMOE::Array:UPDAT>"
					response = Mechanize.new.get("https://www.reddit.com/r/awwnime/new.json?limit=125").body
					@moe = JSON.parse(response)
				end
				index = Random.rand(1..100)
				if (send_img @moe['data']['children'][index]['data']['url'])
					send_link(@moe['data']['children'][index]['data']['url'], @moe['data']['children'][index]['data']['title'])
				else
					send "Reddit server returned a null value; request failed"
				end
				imt.terminate
				Thread.exit
			}
		rescue => e
			puts "Server is not responding"
		end
	end
	
	def potato()
		check_time()
		begin
			imt = Thread.new {
				if (@potato.nil?)
					puts "["+ Time.now.strftime("%H:%M:%S") +"] #<POTATO::Array:UPDAT>"
					response = Mechanize.new.get("https://www.reddit.com/r/FoodPorn/search.json?q=potato&sort=new&restrict_sr=on&limit=125").body
					@potato = JSON.parse(response)
				end
				index = Random.rand(1..100)
				if (send_img @potato['data']['children'][index].nil?)
					send "Reddit server returned a null value"
					return
				end
				if (send_img @potato['data']['children'][index]['data']['url'])
					send_link(@potato['data']['children'][index]['data']['url'], @potato['data']['children'][index]['data']['title'])
				else
					send "Reddit server returned a null value; request failed"
				end
				imt.terminate
				Thread.exit
			}
		rescue => e
			puts e
		end
	end
	
	def food_porn
		begin
			check_time()
			imt = Thread.new {
				if (@food.nil?)
					puts "["+ Time.now.strftime("%H:%M:%S") +"] #<FOODPRN::Array:UPDAT>"
					response = Mechanize.new.get("https://www.reddit.com/r/FoodPorn/new.json?limit=125").body
					@food = JSON.parse(response)
				end
				index = Random.rand(1..100)
				if (@food['data']['children'][index].nil?)
					send "Reddit server returned a null value"
					return
				end
				if (send_img @food['data']['children'][index]['data']['url'])
					send_link(@food['data']['children'][index]['data']['url'], @food['data']['children'][index]['data']['title'])
				else
					send "Reddit server returned a null value; request failed"
				end
				imt.terminate
				Thread.exit
			}
		rescue => e
			puts e
			puts "Error"
		end
	end
	
	def cats ()
		imt = Thread.new {
			if (check_time || @cats.nil?)
				puts "["+ Time.now.strftime("%H:%M:%S") +"] #<CATZ::Array:UPDAT>"
				response = Mechanize.new.get("https://www.reddit.com/r/cats/new.json?limit=125").body
				@cats = JSON.parse(response)
			end
			index = Random.rand(1..100)
			if (@cats['data']['children'][index]['data']['url'].nil?)
				send "Reddit server returned a null value"
				return
			end
			if (send_img @cats['data']['children'][index]['data']['url'])
				send_link(@cats['data']['children'][index]['data']['url'], @cats['data']['children'][index]['data']['title'])
			else
				send "Reddit server returned a null value; request failed"
			end
			imt.terminate
			Thread.exit
		}
	end
	
	def russian_roulette ()
		rusCLI = Mumble::Client.new($hostname, 24859, "Russia-senpai", '')
		rusCLI.connect
		sleep(1)
		rusCLI.join_channel('The Void')
		i = 0
		shot = false
		fire = 0
		tot_shots = 6 + rand(4)
		trigger = Random.rand(1..tot_shots)
		sleep(2)
		rusCLI.text_channel 'The Void', "Beginning russian roulette"
		rusCLI.text_channel 'The Void', "Type <b>anything</b> to play"
		puts "["+ Time.now.strftime("%H:%M:%S") +"] #<RR::TriggerIs:"+trigger.to_s+">"
		puts "["+ Time.now.strftime("%H:%M:%S") +"] #<RR::TtlShotsIs:"+tot_shots.to_s+">"
		rusCLI.on_text_message do |msg|
			log @cli.users[msg.actor].name + ": " + msg.message
			if rusCLI.users.has_key?(msg.actor) && shot != true
				case msg.message.to_s
				when /.*$/
					i += 1
					fire = Random.rand(1..tot_shots)
					puts "["+ Time.now.strftime("%H:%M:%S") +"] #<RR:Fire:" + fire.to_s + ">"
					if (fire != trigger && i != tot_shots)
						rusCLI.text_channel 'The Void',  rusCLI.users[msg.actor].name + " is safe."
					else
						shot = true
						rusCLI.text_channel 'The Void',  rusCLI.users[msg.actor].name + " has been killed!"
						kick_user(msg.actor, "Death results in a kick!")
					end
				end
			else
				rusCLI.disconnect
			end
		end
		until shot == true
		end
		puts "["+ Time.now.strftime("%H:%M:%S") +"] Russian Roulette is now ending"
		rusCLI.disconnect
	end
	
	def sound_board(snd_name)
		@snd = true
		begin
			if (@snd)
				@snd = false
			#	snd_name = snd_name.strip
			#	name = '/home/botmaster/Mumblechan/audio/' + snd_name + '.fifo'
			#	file =	File.join('./audio/',"#{snd_name}.fifo")
			#	file = File.open(name)
			#	if true
			#		puts file
			#		@cli.player.stream_named_pipe(file)
			#		puts "before audio"
			#		#@cli.player.stream_named_pipe('/home/botmaster/Mumblechan/audio/007.fifo')
			#		@snd = true
			#		puts "sent audio"
			#	else
			#		send snd_name + " is not a valid option."
			#		@snd = true
			#	end
			end
		rescue => e
			puts e.message
		end
	end
	
	def ohoho (status)		
		check_time()
		if (@ohoho.nil?)
			ohoho_json = JSON.parse(Mechanize.new.get("https://www.googleapis.com/youtube/v3/search?key=AIzaSyDIRv1WFdkb4BiMT1ltTBfxFPyRl63zWwI&channelId=UCNCUFLgcOm3aYY-iMnRjDAA&part=snippet,id&order=date&maxResults=50").body)
			@ohoho = Array.new
			puts "["+ Time.now.strftime("%H:%M:%S") +"] #<OHOHO::Array:UPDAT>"
			ohoho_json['items'].each do |v|
				video = { "vid" => "https://www.youtube.com/watch?v="+ v["id"]["videoId"], "desc" => v["snippet"]["description"]}
				@ohoho.push(video)
			end
		end
		if (status)
			video = @ohoho.sample
			send_link(video["vid"], "ohoho~: " + video["desc"])
		else
			video = @ohoho[0]
			send_link(video["vid"], "ohoho~: " + video["desc"])
		end
	end
	
	def youtube_title(yt_link)	
		match = nil
		begin
			title = Mechanize.new.get(yt_link).title
			send_link(yt_link,title)
		rescue Mechanize::ResponseCodeError => e
			puts e
			send "Bad link?"
		end
	end
	
	def steam_link (steam_app)
		match = nil
		begin
			page = Mechanize.new.get(steam_app)
			title = page.title
			price = page.at('.game_purchase_price')
			if (price.nil?)
				price = page.at('.discount_final_price')
				if (price.nil?)
					price = "Pre-order"
				else
					price = price.text
				end
			else
				price = price.text.gsub(/(\t|(\r)?\n)/, '')
			end
			description = page.at('.game_description_snippet')
			img = page.at('.game_header_image_full')
			form = page.form_with(:id => 'agecheck_form')
			if (form.nil?)
				if !(title.nil?)
					if !(description.nil?)
						send_img(img['src'])
						send description.to_s.scan(/<div class=\"game_description_snippet\">(.*)<\/div>/m)[0][0]
						send_link(steam_app, title + " | " + price)
					else
						send_link(steam_app, title + " | " + price)
					end
				end
			else
				form = page.forms[1]
				form.ageYear = "1985"
				page = form.submit
				puts "["+ Time.now.strftime("%H:%M:%S") +"] #<STEAMLNK::AgeBypass:OK>"
				title = page.title
				description = page.at('.game_description_snippet')
				img = page.at('.game_header_image_full')
				if !(title.nil?)
					if !(description.nil?)
						send_img(img.to_s.scan(/.*src=\"(.*)\"/i)[0][0])
						send description.to_s.scan(/<div class=\"game_description_snippet\">(.*)<\/div>/m)[0][0]
						send_link(steam_app, title + " | " + price)
					else
						send_link(steam_app, title + " | " + price)
					end
				end
			end
		rescue Mechanize::ResponseCodeError => e
			puts e
			send "Sorry, there was a problem getting the title."
		end
	end
	
	def amazon_link (amzn)
		begin
			if (!(amzn =~ /^https?:\/\/www.amazon.com\/s\/.*$/i))
				page = Mechanize.new.get(amzn)
				title = page.at('#productTitle')
				if title.nil?
					sleep(1)
					secondTitle = page.at('#btAsinTitle')
					title = secondTitle.text.strip
					if title.nil? # not an amazon product page
						console_msg("<CakeZomb::AMZN:FAIL>")
						return
					end
				else
					title = title.text.strip
				end
				price = page.at('#priceblock_ourprice')
				if (price.nil?)
					price = page.at('#priceblock_saleprice')
				end
				
				if (price.nil?)
					price = page.at('#actualPriceValue')
				end
				
				if !(price.nil?)
					send_link(amzn,title + " " + price)
				else
					send_link(amzn,title)
				end
			else
				console_msg "Amazon search link detected."
				update_link("Mumble-chan", amzn)
			end
		rescue Mechanize::ResponseCodeError => e
			puts "["+ Time.now.strftime("%H:%M:%S") +"] #<CakeZomb::AMZN:FAIL>"
			puts e
		end		
	end
	
	def weather (zipcode)
		if (zipcode.nil?)
			zipcode = 95121
		end
		open("http://api.wunderground.com/api/aa3497b292486deb/conditions/q/" + zipcode.to_s + ".json") do |f|
			json_string = f.read
			parsed_json = JSON.parse(json_string)
			weather_lookup(parsed_json)
		end
	end
	
	def weather_loc (city, state, w_api)
		parsed_json = w_api.conditions_for(state, city)
		weather_lookup(parsed_json)
	end
	
	def weather_lookup (parsed_json)
		str = ""
		if (parsed_json.has_key?("current_observation"))
			location = parsed_json['current_observation']['display_location']['full']
			conditions = parsed_json['current_observation']['weather']
			wind_spd = parsed_json['current_observation']['wind_mph']
			wind_gust = parsed_json['current_observation']['wind_gust_mph']
			percip = parsed_json['current_observation']['precip_today_in']
			temp_f = parsed_json['current_observation']['temp_f']
			cond_img = parsed_json['current_observation']['icon_url']
			send "The current weather for: " + location
			send_img(cond_img, "50", "50")
			str << "<br>Conditions: " + conditions
			str << "<br>Wind speed at " + wind_spd.to_s + "mph with gusts up to " + wind_gust.to_s + " mph."
			str << "<br>Current temperature: " + temp_f.to_s + " °F"
			str << "<br>Precipitation: " + percip.to_s + " in"
			send str
			send_img("https://cakezombies.com/memefaces/wunder.jpg")
			send_link("http://www.wunderground.com/?apiref=4ca273e3d5565f51", "Weather provided by wunderground")
		else
			send "Usage: /weather CITY STATE_ABBRV  ...  /weather zip_code"
		end	
	end
	
	def weather_forecast_loc (city, state, w_api)
		parsed_json = w_api.forecast10day_for(state, city)
		weather_forecast_lookup(parsed_json)
	end
	
	def weather_forecast (zipcode, w_api)
		parsed_json = w_api.forecast10day_for(zipcode)
		weather_forecast_lookup(parsed_json)
	end
	
	def weather_forecast_lookup (parsed_json)
		weath = ""
		if (parsed_json.has_key?("forecast"))
			weath << "Here is the weather for the next three days:<br><b>"+parsed_json['forecast']['txt_forecast']['forecastday'][2]['title'] + ":</b><br/>"
			weath << encodeImage(parsed_json['forecast']['txt_forecast']['forecastday'][2]['icon_url'], "50", "50")+"<br/>"
			weath << parsed_json['forecast']['txt_forecast']['forecastday'][2]['fcttext']
			send weath
			weath = " <b>On " + parsed_json['forecast']['txt_forecast']['forecastday'][4]['title'] +":</b><br/>"
			weath << encodeImage(parsed_json['forecast']['txt_forecast']['forecastday'][4]['icon_url'], "50", "50")+"<br/>"
			weath << parsed_json['forecast']['txt_forecast']['forecastday'][4]['fcttext']
			send weath
			weath = " <b> And on " + parsed_json['forecast']['txt_forecast']['forecastday'][6]['title'] +":</b><br/>"
			weath << encodeImage(parsed_json['forecast']['txt_forecast']['forecastday'][6]['icon_url'], "50", "50") +"<br/>"
			weath << parsed_json['forecast']['txt_forecast']['forecastday'][6]['fcttext']
			send weath
			send_img("https://cakezombies.com/memefaces/wunder.jpg")
			send_link("http://www.wunderground.com/?apiref=4ca273e3d5565f51", "Weather provided by wunderground")
		else
			send "Usage: /forecast CITY STATE_ABBRV ... /weather zip_code"
		end
	end
	
	def porn ()
		open("http://api.redtube.com/?data=redtube.Videos.searchVideos&output=json") do |f|
			json_string = f.read
			parsed_json = JSON.parse(json_string)
			i = Random.rand(1..20)
			send_link(parsed_json['videos'][i]['video']['url'], parsed_json['videos'][i]['video']['title'])
			send "this feature is under reconstruction; old functionality maintained for david"
		end
	end
	
	def pornoftheday ()
		begin
			check_time()
			if (@pornday.nil?)
				pornlnk = Mechanize.new.get("http://www.xvideos.com/best/day/0/").search("//a")[24].attributes['href'].text
				pornlnk = "http://xvideos.com" + pornlnk
				porn = Mechanize.new.get(pornlnk)
				porntitle = porn.title
				pornrating = porn.at('#rating').text
				porncount = porn.at('#ratingTotal').text
				@pornday = { "title" => porntitle, "link" => pornlnk, "rating" => pornrating, "count" => porncount }
			end
			send_link(@pornday["link"], @pornday["title"])
			send("Rating: " + @pornday['rating'] + " of " + @pornday['count'] + " votes.")
		rescue Mechanize::ResponseCodeError => e
			puts e
			send "xvideos has denied your porn consumption."
		end
	end
		
	def hangman ()
		hang = Hangman.new()
		hang.runtime()
	end
		
	def initialize
		if ($debug)
			$hostname = "192.168.1.205"
		else
			$hostname = "192.168.1.212"
		end
		@channel = 1
		@userlist = Hash.new(false)
		@userCount = 0
		@cli = Mumble::Client.new($hostname, 24859, "Mumble-chan", '')
		send get_version
		reload()
	end
	
	def uptime ()
		t = (Time.now - IO.read('/proc/uptime').split[0].to_i)
		total = TimeDifference.between(t, Time.now).in_general
		uptime = ""
		if (total[:years] != 0)
			uptime << total[:years].to_s + " years "
		end
		if (total[:months] != 0)
			uptime << total[:months].to_s + " months "
		end
		if (total[:weeks] != 0)
			uptime << total[:weeks].to_s + " weeks "
		end
		if (total[:days] != 0)
			uptime << total[:days].to_s + " days "
		end
		if (total[:hours] != 0)
			uptime << total[:hours].to_s + " hours "
		end
		if (total[:minutes] != 0)
			uptime << total[:minutes].to_s + " minutes "
		end
		str = "<br><b>System running since</b>: " + t.to_s + "<br><b>Uptime</b>: " + uptime + total[:seconds].to_s + " seconds."
		str << "<br>\n<br><b>Running since</b>: " + @startTime.to_s + "<br><b>Mumble-chan's uptime</b>: " + TimeDifference.between(@startTime, Time.now).in_hours.to_s + " hours"
		return str
	end 	
	
	def rhyme_game ()
		i = 0
		rhyme_words = []
		File.open('/home/botmaster/Mumblechan/ping.txt', 'r') do |f|
			f.each_line do |line|
				rhyme_words[i] = line
				i = i + 1
			end
		end
		send rhyme_words.shuffle.sample		
	end
		
	def motd ()
		file = File.open("motd.txt", "rb")
		@motd = file.read
		file.close
	end
	
	def reload
		begin
			puts "["+ Time.now.strftime("%H:%M:%S") + "] Command file reloaded."
			@rld_cmd = JSON.parse(File.read("commands.json"))
		rescue => e
			puts "["+ Time.now.strftime("%H:%M:%S") + "] There was an error reloading the JSON command file"
		end
	end
	
	def dyn_cmd (command)
		cmd = command.downcase
		begin
			t1 = Thread.new {
				if !(@rld_cmd[cmd].nil?)
					if !(@rld_cmd[cmd]["img"].nil?)
						send_img(@rld_cmd[cmd]["img"])
					else
						console_msg("EVAL issued with: " + @rld_cmd[cmd]["eval"])
						eval @rld_cmd[cmd]["eval"]
					end
				else
					puts "["+ Time.now.strftime("%H:%M:%S") + "] Undefined command used: " + command
				end
				t1.terminate
				Thread.exit
			}
		rescue => e
			puts e
			console_msg("A command issued contains bugs")
		end		
	end
	
	def console_msg (msg)
		puts "["+ Time.now.strftime("%H:%M:%S") + "] " + msg
	end
	
	def xkcd
		begin
			page = Mechanize.new.get("http://c.xkcd.com/random/comic/")
			url = page.at("#comic img")['src']
			url = url[2..url.length]
			url = "http://" + url
			send_img(url, "500", "500")
			send "<b>Title</b>: " + page.at("#comic img")['title']
			send_link(url, page.at("#comic img")['alt'])
		rescue Mechanize::ResponseCodeError => e
			puts e
			puts "["+ Time.now.strftime("%H:%M:%S") +"] XKCD image pull error"
		end
	end
	
	def check_time ()
		if (TimeDifference.between(Time.now, @time).in_hours >  12)
			puts "["+ Time.now.strftime("%H:%M:%S") +"] #<CHECKTIME::TIME:UPDAT>"
			@dadjoke = nil
			@potato = nil
			@food = nil
			@ohoho = nil
			@moe = nil
			@pornday = nil
			@time = Time.now
			return false
		else
			puts "["+ Time.now.strftime("%H:%M:%S") +"] #<CHECKTIME::TIME:OK>"
			return true
		end
	end
	
	def urban (query)
		begin
			query.gsub!(/&amp;/,"&")
			query = CGI.escape(CGI.unescape(query))
			link = "https://www.urbandictionary.com/define.php?term=" + query
			page = Mechanize.new.get(link)
			define = page.at('.meaning')
			ex = page.at('.example')
			if !(define.nil?)
				define = define.to_s.scan(/<div class=\"meaning\">\n(.*)\n<\/div>/i)[0][0]
				send_link(link, "Urban: " + query)
				send "Definition: " + define
				puts "["+ Time.now.strftime("%H:%M:%S") +"] #<URBAN::Stat:OK>"
			else
				send "Word is not defined."
			end
		rescue Mechanize::ResponseCodeError => e
			puts "["+ Time.now.strftime("%H:%M:%S") +"] #<URBAN::" + e.message + ">"
			send "Bad query"
		end
	end

	def updateUserlist (username, status)
		if (!($debug))
			if (status)
				@userlist[username] = status
				@userCount++
			else
				@userlist.delete(username)
				@userCount--
			end
			File.open('/home/botmaster/Logs/userlist.txt', 'w') do |f|
				f.write(JSON.generate(@userlist))
			end
		end
	end
	
	def textUser (username, msg)
		begin
			if (@cli.users.has_value?(username))
				console_msg("user is not online")
			else
				@cli.text_user(username, msg)
			end
		rescue Mumble::UserNotFound
			console_msg("User (" + username + ") is not online")
		end
	end
	
	def checkVersion
		if (TimeDifference.between(Time.now, @updtT).in_hours >  48)
			get_version
			send $status
			@updtT = Time.now
		end
	end
	
def runtime ()
	begin
		@time = Time.now
		@updtT = Time.now
		@startTime = Time.now
		count = 0
		$cakezombies = "https://cakezombies.com/memefaces/"
		@start = false
		quit = false
		w_api = Wunderground.new("aa3497b292486deb") # create api hook
		@cli.on_text_message do |msg|
			if @cli.users.has_key?(msg.actor) && (count==0)
				### Check for new version
				checkVersion
				### Check if whisper
				if (!(msg.channel_id.nil?) && @cli.users[msg.actor].channel_id == 1)
					@channel = 1
					log @cli.users[msg.actor].name + ": " + msg.message
					pass = true
				elsif (msg.channel_id.to_s != '[1]')
					@channel = @cli.users[msg.actor].channel_id
					log_whisper @cli.users[msg.actor].name + ": " + msg.message
					pass = true
				else # ignore message to channel if not same channel
					pass = false
				end
				### Parse message
				if (msg.message.length < 500 && pass) #ignore long messages (eg. images)
					case msg.message#.to_s
					when /^\/d (\d{1,5})$/
						send "Rolled for " + Random.rand(1..$1.to_i).to_s 
					when /^\/flip table$/,/^\/ft$/
						send "(ノಠ益ಠ)ノ彡┻━┻"
					when /^\/flip$/,/^\/flip coin$/
						send flip_coin()
					when /^\/help$/,/^\/\?$/
						send "Full list available online: <a href=\"https://cakezombies.com/blog/mumble-chan-commands/?ppt=a851a0b506559cf0fd9d47680598f245\">all commands</a>"
					when /^\/slap (\w{1,20})$/
						slap($1,@cli.users[msg.actor].name)
					when /^\/hungry/,/^\/hunger$/,/^\/golden scripture$/
						send "<a href=\"https://www.dropbox.com/s/q81p4stxqwmd58h/Golden%20Scripture%20clickable.pdf?dl=0\">Golden scripture! By fee</a>"
					when /^\/tip (\w{1,20}) (\d{1,6})$/,/^tip (\w{1,20}) (\d{1,6})$/
						send @cli.users[msg.actor].name + " has given " + $1 + " " + $2.to_s + " internet points!"
					when /^\/splash$/
						send_img("https://i.imgur.com/9oHu0Nb.jpg")
						send "By: " + (@cli.users[msg.actor].name)
					when /^\/dad jokes$/,/^\/dj$/i,/^\/dadjokes$/
						dad_joke()
					when /^\/ping$/
						send "pong!"
					when /^\/me ([\w\s]{3,40})$/
						send "<br><b>*" + @cli.users[msg.actor].name + " " + msg.message + "*</b>"
					when /^\/senpai$/
						send "..."
					when /^\/google (.*)$/,/^\/g (.*)$/
						send_link("https://www.google.com/search?q="+$1, $1)
					when /^\/urban (.*)$/,/^\/urb (.*)$/
						urban($1)
					when /^\/wvffle$/,/^\/waffledrop$/,/^\/waffle drop$/,/^\/waffle$/
						send "<a href=\"https://www.youtube.com/watch?v=kB8pG1-dIAA\"> wvffle drop</a>"
					when /^\/yeah$/i,/^\/yes$/i				
						send_img("https://cakezombies.com/memefaces/fuck_yeah_smile.jpg")
					when /^\/bitch ?slap$/,/^\/bitchslap$/
						send_img("https://cakezombies.com/memefaces/bitchslap.jpg")
					when /^\/russianroulette$/,/^\/russian roulette$/
						if count == 0
							send @cli.users[msg.actor].name + ' has begun russian roulette'
							count = russian_roulette()
							count = 1
							timestamp = Time.now
						else
							send "Currently in game"
						end
					when /^\/sb (\w{3,10})$/
						if ((@cli.users[msg.actor].name == 'Moonlight') || @cli.users[msg.actor].name == 'Syndicate')
							sound_board($1)
						else
							send @cli.users[msg.actor].name + " does not have permission to play sounds."
						end
						send "This feature is still under construction..."
					when /(^carl(.*)?$|(.*)? carl(.*)?$)/i
						send "Fucking Carl!"
					when /^meow$/
						send "Nyaaaa~"
					when /^!quit$/
						if (@cli.users[msg.actor].name == 'Syndicate' || @cli.users[msg.actor].name == 'Moonlight')
							send "(✖╭╮✖)<br>Mumble-chan shutting down!"
							exit
						else
							@cli.text_user(@cli.users[msg.actor].name, "You do not have access to this command!")
						end
					when /^!refresh$/
						if (@cli.users[msg.actor].name == 'Syndicate' || @cli.users[msg.actor].name == 'Moonlight')
							@time = Time.new(2002)
							check_time()
							send "Force update requested; will update on next service call."
						else
							@cli.text_user(@cli.users[msg.actor].name, "You do not have access to this command.")
						end
					when /^.*(\<a href=\"((?:https?:\/\/)?(www\.)?youtu(?:\.be|be\.com)\/(?:watch\?)?([\w&\%\._;\#=\?!-]{10,})?)*\"\>)+((?:https?:\/\/)?(www\.)?youtu(?:\.be|be\.com)\/(?:watch\?)?([\w&\%_;\#\.=\?!-]{10,})?)+(\<\/a\>)?.*$/
						youtube_title($2)
					when /^.*(\<a href=\"http:\/\/store.steampowered.com\/app\/[\d]*\/\"\>)?(http:\/\/store.steampowered.com\/app\/[\d]*\/).*$/
						steam_link($2)
					when /^.*(\<a href=\"http:\/\/www\.amazon\.com\/.*\"\>)?(http:\/\/www\.amazon\.com\/(.*))<\/a>.*$/
						amazon_link($2)
					when /^\/im?g? <a href=\"(.*\.(jpg|png|jpeg|bmp))\">.*(jpg|png|jpeg|bmp)<\/a>( (([2-4][0-9][0-9]) ([2-4][0-9][0-9])|500 500))?/i
						url = $1
						size = $4
						height = $5
						width = $6
						if !(url =~ /.*9gag.*/)
							if (size.nil?)
								send_img(url, "200", "200")
							elsif (!(height.nil? && width.nil?))
								send_img(url, height, width)
							end
						end
					when /^(\<a href=.*)/
						update_link(@cli.users[msg.actor].name, $1)
					when /^herp$/
						send "derp"
					when /^derp$/
						send "herp"
					when /^bored$/,/^\/music$/,/^\/bored$/
						send_link("https://plug.dj/nightblue", "plug.dj - all your music needs!")
					when /^\/afk$/
						send @cli.users[msg.actor].name + " is AFK"
						@cli.send_user_state(session: @cli.users[msg.actor],mute: true, deaf: true)
					when /^\/back$/
						send @cli.users[msg.actor].name + " is no longer AFK."
						@cli.send_user_state(session: @cli.users[msg.actor], mute: false,deaf: true)
					when /^\/whatdoesthefoxsay$/
						send "Nothing."
						@cli.send_user_state(session: @cli.users[msg.actor],mute: true)
					when /^\/buy/
						send "I don't sell shit!"
					when /^\/sell/
						send "I don't buy shit!"
					when /^\/bobalocations$/,/^\/boba$/
						send_link("http://www.yelp.com/search?find_desc=boba&find_loc=San+Jose%2C+CA&ns=1", "boba locations")
					when /^\/umadbro$/,/^\/umad$/
						send_img("https://cakezombies.com/memefaces/umadbro.jpg")
					when /^\/noticeme$/,/^\/noticemesenpai$/
						kick_user(msg.actor, "Oh hai der!")
					when /^\/niggafy$/,/^\/purple$/
						send_img("https://cakezombies.com/memefaces/purple.jpg")
					when /^kick fee$/
						if (!(kick_user_srh("Feeenis", @cli.users[msg.actor].name + " says hai fee <3")))
							kick_user_srh("Fwap", @cli.users[msg.actor].name + " says hai fee <3")
						end
					when /^(who) are you\?$/
						send "I'm Mumble-chan (" + $version + ")!<br>☆*:.｡. o(≧▽≦)o .｡.:*☆"
					when /^\/weather (\d{5})$/,/^\/weather$/i
						weather($1)
					when /^\/weather ([\w\s]+)+, (\w{2})$/
						weather_loc($1, $2, w_api)
					when /^\/weather tomorrow$/,/^\/forecast$/
						weather_forecast(95121, w_api)
					when /^\/forecast (\d{5})$/,/^\/weather tomorrow (\d{5})$/
						weather_forecast($1, w_api)
					when /^\/forecast ([\w\s]+)+, (\w{2})$/,/^\/weather tomorrow ([\w\s]+)+, (\w{2})$/
						weather_forecast_loc($1, $2, w_api)
					when /^\/turndownforwhat$/
						send "<a href=\"https://www.youtube.com/watch?v=otCpCn0l4Wo\">Can't turn it down!</a>"
					when /^\/twerk$/
						send "Useless command."
					when /^\/quiz$/
						send "Not implemented; designing..."
					when /^\/blackjack$/
						send "Not yet implemented. Priority code=4"
					when /^\/hangman$/
						if (count == 0)
							t1=Thread.new{hangman()}
							count = 1
							t1.join
							timestamp = Time.now
						else
							send "Already in game!"
						end
					when /^\/version$/
						get_version
						send $status
					when /^\/pong$/
						send "bong!"
					when /^\/useless$/i
						send "NO YOU'RE USELESS!!"
						kick_user(msg.actor, "with <3, Mumble-chan")
					when /^\/serverinfo$/,/^\/server$/,/^\/cakezombies$/
						send_link("https://cakezombies.com/blog/server", "Server Information")
						send "Users can connect to Mumble using this info:<br><b>host:</b> cakezombies.com<br><b>port:</b> 24859<br><b>username:</b> anything"
					when /^\/kaw+$/i,/^\/a?merica$/i,/^\/kaw!$/
						send_img("https://cakezombies.com/memefaces/merica.jpg")
					when /^\/sparkle$/,/^\/allens ?sparkle ?face$/,/^\/sparkleface$/,/^\/kirakira$/
						send_img("https://cakezombies.com/memefaces/sparkle.jpg")
					when /^\/bong$/
						send "long!"
					when /^\/slurp$/
						send_link("http://media.tumblr.com/387f1e6b3787fe4aedc8d4946aff6c4d/tumblr_inline_moy3675pBw1qz4rgp.gif", "slurp slurp slurp")
					when /^\/hi$/
						send "haihai"
					when /^goodnight$/,/^good ?night ?mumblechan$/
						send "Goodnight!"
					when /^wao!$/
						send "WAO!"
					when /^\/what$/
						send "the hell?!"
					when /^\/upupdowndownleftrightleftrightba$/i
						send_link("http://konamicodesites.com/", "Konami codeeeeeeeeeeee!")
					when /^\/diet$/
						kick_user_srh("Blue", @cli.users[msg.actor].name + " has kicked Vinson! \"diet\"")
					when /^\/prostitute ?vinson$/i,/^\/vinson$/i
						vinson = Random.rand(1..5)
						send_img("https://cakezombies.com/memefaces/cao/" + vinson.to_s + "cao.jpg")
					when /^\/gg$/
						send "qq noob"
					when /^\/clear$/
						i = 3
						while i > 0
							@cli.text_user(@cli.users[msg.actor].name, "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>")
							i = i - 1
						end
					when /^\/motherfuckingmetapod$/i
						send_img("https://cakezombies.com/memefaces/metapod.jpg")
					when /^\/thefuck$/i
						send_img("https://cakezombies.com/memefaces/thefuck.jpg")
					when /^\/fan (\w{1,10})$/
						send "FAN SMACK AGAINST " + $1 + "'s head!"
					when /^\/fap$/,/^\/porn$/
						porn()
					when /^\/pornoftheday$/,/^\/pornlinkoftheday$/
						pornoftheday()
					when /^\/about$/i
						send_img("https://cakezombies.com/memefaces/mumblechan.jpg")
						send "☆*:.｡. o(≧▽≦)o .｡.:*☆ <br> Mumble-chan running " + $version
						send "Running on Debian using the Mumble Ruby API"
						send uptime()
						send "<b>Resources used:</b>"
						send_link("https://github.com/perrym5/mumble-ruby", "Mumble Ruby API")
						send_link("http://wiki.mumble.info/wiki/Main_Page", "Mumble")
					when /^\/history$/,/^\/chathistory$/,/^\/logs?$/
						send_link("https://cakezombies.com/logs/", "This channel is logged, please refer to this link")
						@cli.text_user(@cli.users[msg.actor].name, "ID: potato<br>PW: \"Magical !\" (without quotes)")
						console_msg(@cli.users[msg.actor].name + " has requested log site credentials.")
					when /^\/lazy$/
						send_img("https://cakezombies.com/memefaces/lazy.jpg")
					when /^\/indeed$/
						send_img("https://cakezombies.com/memefaces/indeed.jpg")
					when /^\/magical$/
						send_img("https://cakezombies.com/memefaces/magical.jpg")
					when /^\/alone$/
						send_img("https://cakezombies.com/memefaces/forever_alone_clean.jpg")
					when /^\/accepted$/
						send_img("https://cakezombies.com/memefaces/challenge_accepted.jpg")
					when /^\/holyshit$/
						send_img("https://cakezombies.com/memefaces/cereal_guy_spitting.jpg")
					when /^\/kawaii$/
						send_img("https://cakezombies.com/memefaces/kawaii.jpg")
					when /^\/ohstahp$/i,/^\/ohstop$/i,/^\/stahp$/i
						send_img("https://cakezombies.com/memefaces/stahp.jpg")
					when /^\/facepalm$/,/^\/face palm$/
						facepalm = Random.rand(1..12)
						send_img("https://cakezombies.com/memefaces/facepalms/" + facepalm.to_s + ".jpg")
					when /^\/megusta$/i,/^\/me gusta$/
						send_img("https://cakezombies.com/memefaces/me-gusta.jpg")
					when /^\/lol$/i
						send_img("https://cakezombies.com/memefaces/lol.jpg")
					when /^\/desu~$/,/^\/desu$/i
						send_link("https://cakezombies.com/desu/", "ლ(◕‿‿◕ლ) desu~")
					when /^\/420$/
						send_link("http://420.moe", "420 moe moe")
					when /^\/SIT THE FUCK DOWN$/i
						kick_user_srh("Shiki", "public intoxication")
					when /^\/woo$/i
						send_img ("https://cakezombies.com/memefaces/woo.jpg")
					when /^\/ghostly$/,/^\/scare/
						send_img ("https://cakezombies.com/memefaces/ghostly.jpg")
					when /^i see you$/,/^i know what you did last summer$/,/^\/eye$/
						send_img ("https://cakezombies.com/memefaces/iseeyou.jpg")
					when /^\/sushi$/
						send_img ("https://cakezombies.com/memefaces/sushi.jpg")
					when /^\/floatinghead$/
						send_img ("https://cakezombies.com/memefaces/ghostly.jpg")
					when /^\/swpm$/i,/^\/southwestpattymelt$/
						send_img ("https://cakezombies.com/memefaces/burgs.jpg")
					when /^\/respecttables$/,/^\/pleaserespecttables$/,/^\/unfliptable$/
						send "┬─┬ノ(ಠ_ಠノ)"
					when /^\/flip (\w{2,10})$/
						send "┬─┬ ︵ /(.□. / ) "
					when /^\/unflip (\w{2,10})$/
						send "-( °-°)- ノ(ಠ_ಠノ)"
					when /^\/uptime$/
						send uptime()
					when /^\/Oh heyyy~$/
						kick_user_srh("zzzaap", @cli.users[msg.actor].name + ": heyyy~")
					when /^\/walnut$/
						kick_user_srh("Wolfhall", @cli.users[msg.actor].name + " loves walnuts!")
					when /^\/potato!$/,/^\/billybobfromsouthcarolina$/
						kick_user_srh("Syndicate", @cli.users[msg.actor].name + " is a potato!")
					when /^\/coffee$/
						send_link("https://www.youtube.com/watch?v=GsQDzWfGErY", "The best part of waking up is Folgers!")
					when /^\/sad$/,/^\/cry$/
						send_img("https://cakezombies.com/memefaces/sad.jpg")
					when /^\/badass$/
						send_img("https://cakezombies.com/memefaces/badass.jpg")
					when /^\/why$/
						send_img("https://cakezombies.com/memefaces/why.jpg")
					when /^\/okay$/
						send_img("https://cakezombies.com/memefaces/okay.jpg")
					when /^\/iknowthatfeels$/,/^\/feelsbro$/,/^\/iknowthefeelbro$/
						send_img("https://cakezombies.com/memefaces/feelsbro.jpg")
					when /^\/fired$/
						send_img("https://cakezombies.com/memefaces/fired.jpg")
					when /^\/burnheal$/
						send_img("https://cakezombies.com/memefaces/burnheal.jpg")
					when /^\/abandoned$/
						send_img("https://cakezombies.com/memefaces/abandoned.jpg")
					when /^\/feels$/
						send_img("https://cakezombies.com/memefaces/feels.jpg")
					when /^\/silky$/
						send_img("https://cakezombies.com/memefaces/silky.jpg")
					when /^\/itsok$/,/^\/itsokbro$/
						send_img("https://cakezombies.com/memefaces/itsokbro.jpg")
					when /^\/doge$/
						send_img("https://cakezombies.com/memefaces/doge.jpg")
					when /^\/boobies$/,/^\/boobs$/
						send_img("https://cakezombies.com/memefaces/boobs.jpg")
					when /^\/WHAT\?!$/i,/^\/WHAT$/i
						send_img("https://cakezombies.com/memefaces/cereal_guy_spitting.jpg")
					when /^\/fu+(ck)?$/i
						send_img("https://cakezombies.com/memefaces/fuuuuuuuu.jpg")
					when /^\/wat$/i
						send_img("https://cakezombies.com/memefaces/wat.jpg")
					when /^so hot$/,/^\/hot$/
						send_img("https://cakezombies.com/memefaces/icecream.jpg")
					when /^\.{3}$/
						send_img("https://cakezombies.com/memefaces/pokerface_clean.jpg")
					when /^\/likeasir$/,/^\/fancy$/
						send_img("https://cakezombies.com/memefaces/like_a_sir.jpg")
					when /^\/itsatrap$/,/^\/trap$/
						send_img("https://cakezombies.com/memefaces/Atrapitis.jpg")
					when /^\/derp$/
						send_img("https://cakezombies.com/memefaces/jesus.jpg")
					when /^\/yuno$/
						send_img("https://cakezombies.com/memefaces/y_u_no.jpg")
					when /^bluescreen$/, /^\/pcded$/,/^bsod$/
						send_img("https://cakezombies.com/memefaces/angry_with_pc.jpg")
					when /^\/cookies?$/
						send_img("https://cakezombies.com/memefaces/cookie.JPG")
					when /^\/coolcat$/,/^\/cats?$/i
						cats()
					when /^\/deathstare$/
						send_img("https://cakezombies.com/memefaces/luigi.jpg")
					when /^\/potato$/i
						potato()
					when /^\/suchwow$/i
						send_img("https://cakezombies.com/memefaces/suchwow.jpg")
					when /^(\/r\/\w{1,20})/
						send_link("https://reddit.com" + $1, $1)
					when /^\/vinsoncoon$/i,/^\/vinson coon$/i
						send_img("https://cakezombies.com/memefaces/vinsoncoon.jpg")
					when /^\/cao$/i
						send_img("https://cakezombies.com/memefaces/thecao.jpg")
					when /^\/sexycat$/i
						send_img("https://cakezombies.com/memefaces/allen_cat.jpg")
					when /^\/takemymoney\!?$/i,/^\/shutupandtakemymoney\!?$/i,/^\/shut up and take my money\!?/i
						send_img("https://cakezombies.com/memefaces/takemymoney.jpg")
					when /^\/disappoint$/i,/^\/iamdisappoint$/i,/^\/soniamdisappoint$/i
						send_img($cakezombies + "disappoint.jpg")
					when /^\/moe$/i,/^\/awwnime$/
						moe()
					when /^\/food$/i,/^\/foodporn$/i
						food_porn
					when /^\/palmface$/i
						send_img($cakezombies + "palmface.jpg")
					when /^\/ohoho( .*| new)?$/i
						if ($1 == " new")
							ohoho(false)
						else
							ohoho(true)
						end
					when /^\/mwahaha$/i
						send_img($cakezombies + "mwhaha.jpg")
					when /^\/360noscope$/i,/^\/mlg$/i
						send_link("https://www.youtube.com/watch?v=gEJHrmliVQw", "360 No Plow | No Scope")
					when /^\/xkcd$/i
						xkcd
					when /^\/reload$/i
						if ((@cli.users[msg.actor].name == 'Moonlight') || @cli.users[msg.actor].name == 'Syndicate')
							reload
							@cli.text_user(@cli.users[msg.actor].name, "Command file reloaded.")
						end
					when /^\/\w{1,4}ong$/
						rhyme_game()
					when /^!say (.*)$/
						if ((@cli.users[msg.actor].name == 'Moonlight') || @cli.users[msg.actor].name == 'Syndicate')
							send $1
							console_msg ("Syndicate sent a say command: " + $1)
						end
					when /^\/caoshit( (.*))?$/i
						annotate_img($cakezombies + "caoshit.jpg", $2, "caoshit")
					when /^\/happy( (.*))?$/i
						annotate_img($cakezombies + "caohappy.jpg", $2, "sohappy")
					when /^\/(.*)$/i
						dyn_cmd($1)
					end
				end
			elsif count != 0
				log @cli.users[msg.actor].name + ": " + msg.message
				if (TimeDifference.between(Time.now, timestamp).in_seconds >  5)
					count = 0
				end
			end
		end
		## log connects/disconnects
		@cli.on_user_state do |message|
			if (@start)
					if (message['name'] != nil)
						updateUserlist(message['name'], true)
						log message['name'] + " connected."
						puts "["+ Time.now.strftime("%H:%M:%S") +"] " + message['name'] + " connected."
					end
			end
		end
		@cli.on_user_remove do |message|
			if (@start)
				if (message.has_key?('ban'))
					log @cli.users[message['session']].name + " was kicked and banned from the server (Reason: " + message['reason'] + ")"
					puts @cli.users[message['session']].name + " was kicked and banned from the server (Reason: " + message['reason'] + ")"
					updateUserlist(@cli.users[message['session']].name, false)
				elsif (message.has_key?('reason'))
					log @cli.users[message['session']].name + " was kicked from the server (Reason: " + message['reason'] + ")"
					puts @cli.users[message['session']].name + " was kicked from the server (Reason: " + message['reason'] + ")"
					updateUserlist(@cli.users[message['session']].name, false)
				else	
					user = @cli.users[message['session']]
					if (user != nil)
						updateUserlist(@cli.users[message['session']].name, false)
						log user.name + " disconnected."
						puts "["+ Time.now.strftime("%H:%M:%S") +"] " + user.name + " disconnected."
					end
				end
			end
		end
		## 
		@cli.connect
		sleep(1)
		@cli.join_channel('The Void')
		### BOT ONLINE.
		#send "☆*:.｡. o(≧▽≦)o .｡.:*☆ <br> Mumble-chan online! Running " + $version + "<br>/help for more"
		send $status
		puts $status
		img = ""
		File.open("./mumbleav.jpg", 'r') { |file| img << file.read }
		@cli.send_user_state(session: @cli.me.session, 
			comment: "Mumble-chan running " + $version + "<br><br><a href=\"https://cakezombies.com/redirect/mumble\">all commands</a><br><a href=\"https://cakezombies.com/logs\">I log the channel I'm in!</a>", 
			texture: img, 
			texture_hash: "1bf588724efeacd19034fea8ea83c1b4ee0f01a4")
		## Get online user list
		hash = @cli.users
		hash.each do |key, user|
			updateUserlist(user.name, true)
		end
		@start = true
		puts "Took " + TimeDifference.between(@startTime, Time.now).in_seconds.to_s + " seconds to start."
		puts "Enter commands below"
		until quit == true			
			command = gets
			case command
			when /^say (.*)$/
				send $1
			when /^down$/,/^down "(.*)"$/
				if ($1.nil?)
					str = "The server will be shutting down. You will be momentarily moved to the backup server if available."
				else
					str = $1
				end
				send str
				@cli.text_channel 'The Crevice', str
				@cli.text_channel 'Dark Alley', str
				@cli.text_channel 'Funky Town', str
				@cli.text_channel 'Wildcard', str
				quit = true
			when /^quit$/i,/^stop$/i,/^exit$/i
				quit = true
				puts "Bye bye!"
			when /^test ?(.*)$/,/^t ?(.*)$/i
				annotate_img("https://cakezombies.com/memefaces/caoshit.jpg", "SHIT IN MY WALLET")
			when /^help$/
				puts "Usage: /say /down /test /reload /uptime /quit"
			when /^uptime$/
				puts uptime()
			when /^reload$/
				reload
			else
				puts "Unknown command: " + command
			end
		end
		@cli.disconnect
		exit
	rescue StandardError => e
		console_msg("Error! " + e.message)
		restart()
	rescue Mumble => e
		puts e.message
	end #rescue
	end
end
bot = MumbleChan.new()
bot.runtime()