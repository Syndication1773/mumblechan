require 'rubygems'
require 'mumble-ruby'


class Hangman
	def send(msg)
		@hangCLI.text_channel 'The Void', msg
	end	

	def initialize ()
		@hangCLI = Mumble::Client.new($hostname, 24859, "Hangman", '')
		@hangCLI.connect
		sleep(1)
		@hangCLI.join_channel('The Void')
	end
	
	def runtime ()
		words = File.open('test.txt')
		words_array = words.readlines
		words_array.shuffle!
	
		game_over = false
		word_guess = ""
		alpha = ""
	
		words_array.shuffle!
		word = words_array[0]
		word_array = word.chars.to_a
		letters_remaining = words_array[0].chars.to_a

		word_array.delete_at(word_array.length-1)
		letters_remaining.delete_at(letters_remaining.length-1)

		alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", 
		"L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
		lives = 10  

		word_array.each do |x|
			word_guess << "_ "
		end
		alphabet.each do |x|
			alpha << "#{x} "
		end
		
		send "Beginning Hangman Mode! Enter <b>uppercase</b> letters to play!<br>The topic is: Dictionary"
		
		send word_guess + "<br>Lives Remaining: #{lives}<br>Letters Remaining: " + alpha		  
		@hangCLI.on_text_message do |msg|
			word_guess = ""
			alpha = ""
			case msg.message.to_s	
			when /^!end$/
				game_over = true
			when /^\/guess \w{1,20}$/
				guess = $1
				if (guess.casecmp(word) == 0)
					send "You win " + @hangCLI.users[msg.actor].name + "! You got the word " + word.upcase
					game_over = true
				else
					send "Sorry! That's not the word!"
					send "I would kick you but not right now"
				end
			when /^(\w{1})$/
				guess = $1
				if alphabet.include?(guess) == false
					@hangCLI.text_user(@hangCLI.users[msg.actor].name, "Try a different letter.")
				elsif lives != 0 && letters_remaining != []		  
					if word_array.include?(guess.downcase) == true
						alphabet.delete(guess)  
						letters_remaining.delete(guess.downcase)
						if (letters_remaining == []) # break early
							send "You got it " + @hangCLI.users[msg.actor].name + "! You got the word " + word.upcase
							game_over = true
						else
							word_array.each do |x|
								if alphabet.include?(x.upcase) == true
									word_guess << "_ "
								else 
									word_guess << "#{x.upcase} "
								end
							end
							alphabet.each do |x|
								alpha << "#{x} "
							end
							send word_guess + "<br>Lives Remaining: #{lives}<br>Letters Remaining: " + alpha	
						end
					elsif lives > 1
						lives -= 1
						send "The letter " + guess + " is not in the word!"
						alphabet.delete(guess)
			  
						word_array.each do |x|
							if alphabet.include?(x.upcase) == true
								word_guess << "_ "
							else 
								word_guess << "#{x.upcase} "
							end
						end
						alphabet.each do |x|
							alpha << "#{x} "
						end
						send word_guess + "<br>Lives Remaining: #{lives}<br>Letters Remaining: " + alpha	  
					else 
						send "Game over!"
						send "The word was " + word.upcase
						game_over = true
					end  
				else
					send "You got it " + @hangCLI.users[msg.actor].name + "! You got the word " + word.upcase
					game_over = true
				end
			end   
		end	  
	
	# prevent thread from ending too soon
	until game_over == true
	end
	words.close
	@hangCLI.disconnect
	end
	
end