# README #

Mumble-chan is a ruby mumble client based off the [Mumble-ruby](https://github.com/mattvperry/mumble-ruby) API by perrym5 and [mumblebot](https://github.com/erulabs/mumblebot) by erulabs.

## Dependencies ##
* Mumble-ruby
* net/https
* uri
* json
* open-uri
* wunderground
* time_difference
* thread
* mechanize
* mini_magick
* youtube_it